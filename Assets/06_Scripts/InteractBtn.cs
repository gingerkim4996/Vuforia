using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InteractBtn : MonoBehaviour
{
    public Animator animator;
    public GameObject Txt;

    void Start()
    {
        Button button = GetComponent<Button>();
        button.onClick.AddListener(PlayDanceAnimation);
    }

    void PlayDanceAnimation()
    {
        animator.SetTrigger("Dance");
        StartCoroutine(ActivateTxt());
    }

    IEnumerator ActivateTxt()
    {
        // 텍스트 활성화
        Txt.SetActive(true);

        yield return new WaitForSeconds(2.5f);

        // 텍스트 비활성화
        Txt.SetActive(false);
    }
}