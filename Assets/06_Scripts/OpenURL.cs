using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenURL : MonoBehaviour
{
    public void Metro4URL()
    {
        Application.OpenURL("https://www2.humetro.busan.kr/homepage/english/page/subLocation.do?menu_no=100105040204");
    }

    public void InstaURL()
    {
        Application.OpenURL("https://www.instagram.com/dduji_busan/");
    }
}
